##GROUP AND COUNT##

Simple tool that counts the frequency of its inputs.  
My first use for it was to find the most accessed files while tailing file accessed


###USAGE###
`tail -f my.log | python group_and_count.py`

```
[100] Hello This is the most frequently logged line
[80] 2nd most frequently logged line
[55] third most, etc, etc
```
  
The number in [] is the number of occurances.  
You need to press Ctl+C for the script to calculate the finally tallys.  
This means that the whatever is piping into this script needs to still be running as well (e.g. tailing something)
