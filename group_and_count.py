#!/usr/bin/env python
import sys
import signal
import operator

counter_map = {}
number_of_entries = 10

if len(sys.argv) > 1:
    number_of_entries = int(sys.argv[1])
    print("Will calculate top %i") % (number_of_entries)

def signal_handler(sig, frame):
        print("Calculating output for %s items") % (len(counter_map))
        sorted_entries = sorted(counter_map.items(), key=operator.itemgetter(1), reverse=True)
        taken_list = sorted_entries[:number_of_entries]
        for item in taken_list:
            print("[%i] %s" % (item[1], item[0].strip()))
        print("Completed")
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

unique_values = 0
total_values = 0
print("Listening to stdin")

for line in sys.stdin:
    total_values += 1
    if line in counter_map.keys():
        counter_map[line] = counter_map[line] + 1
    else:
        unique_values += 1
        counter_map[line] = 1

    totals_str=("collecting... unique: %i, total: %i" % (unique_values, total_values))
    sys.stdout.write(totals_str)
    sys.stdout.write('\r')
    sys.stdout.flush()